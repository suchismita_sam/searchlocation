//
//  ViewController.m
//  SearchLocation
//
//  Created by Click Labs134 on 10/14/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *cities;
NSMutableArray *result;
UITableViewCell *cell;
int i;
MKPointAnnotation *annotation1;
MKPointAnnotation *annotation2;
MKPointAnnotation *annotation3;
MKPointAnnotation *annotation4;
MKPointAnnotation *annotation5;
MKPointAnnotation *annotation6;
MKPointAnnotation *annotation7;
MKPointAnnotation *annotation8;
MKPointAnnotation *annotation9;
CLLocationManager *manager;
NSString *country;


@interface ViewController ()
@property (strong, nonatomic) IBOutlet MKMapView *appleMapView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchLocation;
@property (strong, nonatomic) IBOutlet GMSMapView *googleMap;
@property (strong, nonatomic) IBOutlet UITableView *citiesNames;
@property (strong, nonatomic) IBOutlet UILabel *cityNameLabel;



@end

@implementation ViewController

@synthesize appleMapView;
@synthesize searchLocation;
@synthesize googleMap;
@synthesize citiesNames;
@synthesize cityNameLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Cities Names
    cities=[NSMutableArray arrayWithObjects:@"Chandigarh",@"Delhi",@"Mumbai",@"Chennai",@"Bhubaneshwar",@"Kolkata",@"Noida",@"Bengaluru",nil];
    result=[NSMutableArray arrayWithArray:cities];
    [citiesNames reloadData];
    googleMap.delegate=self;
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];
    
    // Do any additional setup after loading the view, typically from a nib.
}

//CURRENT LOCATION
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation

{
    //CREATES MARKER IN GOOGLE MAP
    CLLocation *currentLocation = newLocation;
    GMSMarker *mark = [GMSMarker markerWithPosition:currentLocation.coordinate];
    mark.title = @"current loation";
    mark.map = googleMap;
    mark.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
    
    
    //CREATES MARKER IN APPLE MAP
    annotation9 = [[MKPointAnnotation alloc] init];
    annotation9.coordinate = currentLocation.coordinate;
    annotation9.title = @"current location";
    [appleMapView addAnnotation:annotation9];
    [self.appleMapView setShowsUserLocation:YES];
    //[manager stopUpdatingLocation ] ;
    
    
    //REVERSE GEOCODER
    MKReverseGeocoder *geoCoder = [[MKReverseGeocoder alloc] initWithCoordinate:currentLocation.coordinate];
    geoCoder.delegate = self;
    [geoCoder start];
}





- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark

{
    
    MKPlacemark * myPlacemark = placemark;
    NSString *city = myPlacemark.thoroughfare;
    NSString *locality=myPlacemark.locality;
    NSString *countryCode=myPlacemark.countryCode;
    
}

//tableview delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [result count];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *resultPredicate=[NSPredicate predicateWithFormat: @"SELF contains[c] %@",searchText];
    result=[NSMutableArray arrayWithArray:[cities filteredArrayUsingPredicate:resultPredicate]];
    citiesNames.hidden=NO;
    [citiesNames reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [tableView cellForRowAtIndexPath:indexPath];
    cell=[tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.textLabel.text=result[indexPath.row];
    cell.textColor=[UIColor whiteColor];
    return cell;
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

//setting marker location
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [googleMap clear];
    if (indexPath.row==0)
    {
        //google map
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(30.75, 76.78);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Chandigarh";
        marker.map = googleMap;
        
        //apple map
        annotation1=[[MKPointAnnotation alloc]init];
        annotation1.coordinate=position;
        annotation1.title=@"Chandigarh";
        [appleMapView addAnnotation:annotation1];
    }
    else if(indexPath.row==1)
    {
        //google map
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(28.6139, 77.2090);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Delhi";
        marker.map = googleMap;
        
        //apple map
        annotation2=[[MKPointAnnotation alloc]init];
        annotation2.coordinate=position;
        annotation2.title=@"Delhi";
        [appleMapView addAnnotation:annotation2];
    }
    else if(indexPath.row==2)
    {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(18.9750, 72.8258);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Mumbai";
        marker.map = googleMap;
        
        //apple map
        annotation3=[[MKPointAnnotation alloc]init];
        annotation3.coordinate=position;
        annotation3.title=@"Mumbai";
        [appleMapView addAnnotation:annotation3];
    }
    else if(indexPath.row==3)
    {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(13.0827, 80.2707);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Chennai";
        marker.map = googleMap;
        
        //apple map
        annotation4=[[MKPointAnnotation alloc]init];
        annotation4.coordinate=position;
        annotation4.title=@"Chennai";
        [appleMapView addAnnotation:annotation4];
    }
    else if(indexPath.row==4)
    {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(20.2700, 85.8400);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Bhubaneshwar";
        marker.map = googleMap;
        
        //apple map
        annotation5=[[MKPointAnnotation alloc]init];
        annotation5.coordinate=position;
        annotation5.title=@"Bhubaneshwar";
        [appleMapView addAnnotation:annotation5];
    }
    else if(indexPath.row==6)
    {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(28.5700, 77.3200);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Noida";
        marker.map = googleMap;
        //apple map
        annotation6=[[MKPointAnnotation alloc]init];
        annotation6.coordinate=position;
        annotation6.title=@"Noida";
        [appleMapView addAnnotation:annotation6];
    }
    else if(indexPath.row==5)
    {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(22.5667, 88.3667);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Kolkata";
        marker.map = googleMap;
        //apple map
        annotation7=[[MKPointAnnotation alloc]init];
        annotation7.coordinate=position;
        annotation7.title=@"Kolkata";
        [appleMapView addAnnotation:annotation7];
    }
    else if(indexPath.row==7)
    {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(12.9667, 77.5667);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = @"Bengaluru";
        marker.map = googleMap;
        //apple map
        annotation8=[[MKPointAnnotation alloc]init];
        annotation8.coordinate=position;
        annotation8.title=@"Bengaluru";
        [appleMapView addAnnotation:annotation8];
    }
}

-(NSString *) getlocation: (CLLocationCoordinate2D )newLocation
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *nlocation = [[CLLocation alloc]initWithLatitude:newLocation.latitude
                                                      longitude:newLocation.longitude];
    
    [geocoder reverseGeocodeLocation :nlocation
                    completionHandler:^(NSArray *placemarks, NSError *error)
     {
         
         if (error)
         {
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         if (placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark = placemarks[0];
             NSDictionary *addressDictionary =
             placemark.addressDictionary;
             NSArray *show= addressDictionary[@"FormattedAddressLines"];
             country = [show componentsJoinedByString:@"," ] ;
             NSLog(@"%@",country);
             GMSMarker *markerall = [GMSMarker markerWithPosition:newLocation];
             markerall.title = country;
             markerall.map = googleMap;
             markerall.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
         }
     }];
    return country;
}


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self getlocation:coordinate];
    [googleMap clear];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
