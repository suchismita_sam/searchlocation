//
//  ViewController.h
//  SearchLocation
//
//  Created by Click Labs134 on 10/14/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface ViewController :
UIViewController

<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,GMSMapViewDelegate>
@end

